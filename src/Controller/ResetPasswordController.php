<?php

namespace App\Controller;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User;
use App\Form\ChangePasswordFormType;
use App\Form\ResetPasswordRequestFormType;
use App\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface as HasherUserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

#[Route('/reset-password')]
/**
 *
 */
class ResetPasswordController extends AbstractController
{
    use ResetPasswordControllerTrait;

    private ResetPasswordHelperInterface $resetPasswordHelper;
    private HasherUserPasswordHasherInterface $_passwordHasher;

    public function __construct(ResetPasswordHelperInterface $resetPasswordHelper, HasherUserPasswordHasherInterface $passwordHasher)
    {
        $this->resetPasswordHelper = $resetPasswordHelper;
        $this->_passwordHasher= $passwordHasher;
    }

    /**
     * Display & process form to request a password reset.
     * @throws TransportExceptionInterface
     *
     * @Route("/api/forgot", name="api_forgot_password", methods={"POST"})
     */
    public function request(Request $request, MailerInterface $mailer): Response
    {
        return $this->processSendingPasswordResetEmail(
            json_decode($request->getContent())->email,
            $mailer
        );
    }

    /**
     * Confirmation page after a user has requested a password reset.
     */
    #[Route('/check-email', name: 'app_check_email')]
    public function checkEmail(): Response
    {
        // Generate a fake token if the user does not exist or someone hit this page directly.
        // This prevents exposing whether or not a user was found with the given email address or not
        if (null === ($resetToken = $this->getTokenObjectFromSession())) {
            $resetToken = $this->resetPasswordHelper->generateFakeResetToken();
        }

        return $this->render('reset_password/check_email.html.twig', [
            'resetToken' => $resetToken,
        ]);
    }

    /**
     * Validates and process the reset URL that the user clicked in their email.
     *
     * @Route("/api/reset", name="api_reset_password", methods={"POST"})
     */
    public function reset(Request $request, UserPasswordHasherInterface $userPasswordHasher): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        try {
            $user = $this->resetPasswordHelper->validateTokenAndFetchUser($data['token']);

        } catch (ResetPasswordExceptionInterface $e) {
            $this->addFlash('reset_password_error', sprintf(
                'There was a problem validating your reset request - %s',
                $e->getReason()
            ));

            return new JsonResponse(
                [
                    'status' => $e->getReason(),
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        // The token is valid; allow the user to change their password
        if ($data['password'] == $data['password_confirm']){
            // A password reset token should be used only once, remove it.
            $this->resetPasswordHelper->removeResetRequest($data['token']);

            // Encode the plain password, and set it.
            $encodedPassword = $this->_passwordHasher->hashPassword(
                $user,
                $data['password']
            );

            $user->setPassword($encodedPassword);
            $this->getDoctrine()->getManager()->flush();

            // The session is cleaned up after the password has been changed.
            $this->cleanSessionAfterReset();

            return new JsonResponse(
                [
                    'status' => 'Password changed successfully !',
                ],
                Response::HTTP_CREATED
            );
        }


        return new JsonResponse(
            $data,
        JsonResponse::HTTP_CREATED
        );
    }

    /**
     * @throws TransportExceptionInterface
     */
    private function processSendingPasswordResetEmail(string $emailFormData, MailerInterface $mailer): JsonResponse
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $emailFormData,
        ]);

        // Do not reveal whether a user account was found or not.
        if (!$user) {
            return new JsonResponse(['Error' => 'Email not found ! Please check your input value.'], 401);
        }


        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface $e) {
            // If you want to tell the user why a reset email was not sent, uncomment
            // the lines below and change the redirect to 'app_forgot_password_request'.
            // Caution: This may reveal if a user is registered or not.

            return new JsonResponse(['Error' => 'An error is happened during process ! Please try again later.']);
        }

        $email = (new TemplatedEmail())
            ->from(new Address('no-reply@local.dev', 'Bocasay_ Password Request'))
            ->to($user->getEmail())
            ->subject('Reset your Bocasay password')
            ->htmlTemplate('reset_password/email.html.twig')
            ->context([
                'resetToken' => $resetToken,
                'user' => [
                    'email' => $user->getEmail(),
                    'name' => $user->getName()
                ]
            ])
        ;

        $mailer->send($email);

        // Store the token object in session for retrieval in check-email route.
        $this->setTokenObjectInSession($resetToken);

        return $this->json($email, $status = 200, $headers = [], $context = []);
    }
}
