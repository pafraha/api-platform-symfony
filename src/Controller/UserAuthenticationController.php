<?php

namespace App\Controller;

use App\Entity\User;
use Lcobucci\JWT\Encoder;
use Lcobucci\JWT\Token;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTEncodedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Namshi\JOSE\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use function Symfony\Component\String\u;

class UserAuthenticationController extends AbstractController
{
    /**
     * Display & process form to request a password reset.
     *
     * @Route("/api/user/login", name="api_login", methods={"POST"})
     * @throws \JsonException
     */
    public function index(Request $request)
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $user = $this->getDoctrine()->getRepository(User::class)->findBy(['username' => $data['username']]);
        $user = $user[0];

        return new JsonResponse([
            'user' => [
                'username' => $user->getUsername(),
                'name' => $user->getName(),
                'email' => $user->getEmail()
            ]
        ], 200);
    }
}
