<?php

namespace App\DataPersister;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface as HasherUserPasswordHasherInterface;

/**
 *
 */
class UserDataPersister implements ContextAwareDataPersisterInterface
{
    private EntityManagerInterface $_entityManager;
    private HasherUserPasswordHasherInterface $_passwordHasher;

    /**
     * @param EntityManagerInterface $entityManager
     * @param HasherUserPasswordHasherInterface $passwordHasher
     */
    public function __construct(EntityManagerInterface $entityManager, HasherUserPasswordHasherInterface $passwordHasher)
    {
        $this->_entityManager = $entityManager;
        $this->_passwordHasher= $passwordHasher;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof User;
    }

    /**
     * @param User $data
     */
    public function persist($data, array $context = [])
    {
        if ($data->getPlainPassword()) {
            $data->setPassword(
                $this->_passwordHasher->hashPassword(
                    $data, $data->getPlainPassword()
                )
            );

            $data->eraseCredentials();
        }

        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = []): void
    {
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}